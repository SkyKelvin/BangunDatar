package com.binaracademy.bangundatar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.binaracademy.bangundatar.databinding.ActivityPersegiPanjangBinding

class PersegiPanjangActivity : AppCompatActivity() {
    private val binding by lazy {ActivityPersegiPanjangBinding.inflate(layoutInflater)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}