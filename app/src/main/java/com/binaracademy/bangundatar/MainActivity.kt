package com.binaracademy.bangundatar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.binaracademy.bangundatar.databinding.ActivityMainBinding
import com.binaracademy.bangundatar.databinding.ActivityPersegiPanjangBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button1.setOnClickListener {
            Toast.makeText(applicationContext, "Bangun Datar 1", Toast.LENGTH_LONG).show()
//            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
        }

        binding.button2.setOnClickListener {
            Toast.makeText(applicationContext, "Bangun Datar 2", Toast.LENGTH_LONG).show()
//            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
        }

        binding.button3.setOnClickListener {
            Toast.makeText(applicationContext, "Bangun Datar 3", Toast.LENGTH_LONG).show()
//            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
        }

        binding.button4.setOnClickListener {
            Toast.makeText(applicationContext, "Bangun Datar 4", Toast.LENGTH_LONG).show()
//            val intent = Intent(this@MainActivity, SecondActivity::class.java)
            startActivity(intent)
        }
    }

}